<?php

namespace App\User;

if(!isset($_SESSION['message'])){
	session_start();
}


use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;

class User extends DB{

	public $id = "";
	public $first_name = "";
	public $last_name = "";
	public $email = "";
	public $password = "";

	public function __construct(){
		parent::__construct();

	}

	public function prepare($data=Array()){

		if(array_key_exists('first_name', $data)){
			$this->first_name = $data['first_name'];
		}
		if(array_key_exists('last_name', $data)){
			$this->last_name = $data['last_name'];
		}
		if(array_key_exists('email', $data)){
			$this->email = $data['email'];
		}
		if(array_key_exists('password', $data)){
			$this->password = md5($data['password']);
		}
		if(array_key_exists('id', $data)){
			$this->id = $data['id'];
		}

		return $this;
	}

	public function store(){

		$query = "INSERT INTO `users` (`first_name`, `last_name`, `email`, `password`) VALUES ('{$this->first_name}', '{$this->last_name}', '{$this->email}', '{$this->password}')";
		$result = mysqli_query($this->conn, $query);

		if($result){
				Message::message("<div class=\"alert alert-success\">
 				 <strong>Success!</strong> Data has been registered successfully.
					</div>");
				Utility::redirect('../../index.php');
			}
			else{
				Message::message("<div class=\"alert alert-danger\">
 				 <strong>Oops!</strong> Data has not been registered successfully.
					</div>");
				Utility::redirect('../../index.php');

				}

	}
	
}
