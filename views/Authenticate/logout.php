<?php

include_once('../../vendor/autoload.php');

session_start();

use App\Utility\Utility;
use App\User\User;
use App\User\Auth;
use App\Message\Message;

$auth = new Auth();
$status=$auth->prepare($_POST)->logout();

if($status){

	return Utility::redirect('../../index.php');
}




 